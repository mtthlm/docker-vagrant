#!/bin/bash
# ------------------------------------------------------------------------------
# CONFIGURATION
# ------------------------------------------------------------------------------

LOG_FILE=/vagrant/provisioner.log
LOG_MAX_LINES=10000

# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------

function file_lines {
  [ -f "$1" ] && echo $(wc -l < "$1") || return 0
}

function timestamp {
  date '+%Y-%m-%d %H:%M:%S'
}

function log {
  echo -e -n "$1" >> $LOG_FILE
}

function log_ln {
  log '['"$(timestamp)"'] '"$1"'\n'
}

function shout {
  echo "$1"
  log_ln "$1"
}

function set_time_zone {
  [ $(timedatectl status | grep 'Time zone:' | awk '{print $3}') == "$1" ] && {
    log_ln 'Time zone already set to '"'""$1""'"
    return 1
  } || {
    timedatectl set-timezone "$1" >> $LOG_FILE 2>&1 && {
      shout 'Set time zone to '"'""$1""'"
      return 0
    } || {
      shout 'Unable to set time zone to '"'""$1""'"
      return 1
    }
  }
}

function add_user_to_group {
  id -Gn "$1" 2>/dev/null | grep -wq "$2" && {
    log_ln 'User '"'""$1""'"' already exists in group '"'""$2""'"
    return 1
  } || {
    usermod -G "$2" -a "$1" >> $LOG_FILE 2>&1 && {
      shout 'Added user '"'""$1""'"' to group '"'""$2""'"
      return 0
    } || {
      shout 'Unable to add user '"'""$1""'"' to group '"'""$2""'"
      return 1
    }
  }
}

function add_repo {
  local REPO="${1##*/}"
  [ -f '/etc/yum.repos.d/'"$REPO" ] && {
    log_ln 'Repo '"'""$REPO""'"' already exists'
    return 1
  } || {
    yum-config-manager --add-repo "$1" >> $LOG_FILE 2>&1 && {
      shout 'Added repo '"'""$REPO""'"
      return 0
    } || {
      shout 'Unable to add repo '"'""$REPO""'"
      return 1
    }
  }
}

function make_yum_cache {
  yum makecache fast >> $LOG_FILE 2>&1 && {
    shout 'Made YUM cache'
    return 0
  } || {
    shout 'Unable to make YUM cache'
    return 1
  }
}

function clean_yum_cache {
  yum clean all >> $LOG_FILE 2>&1 && {
    shout 'Cleaned YUM cache'
    return 0
  } || {
    shout 'Unable to clean YUM cache'
    return 1
  }
}

function update_system {
  yum -y update >> $LOG_FILE 2>&1 && {
    shout 'Updated the system'
    return 0
  } || {
    shout 'Unable to update the system'
    return 1
  }
}

function install_package {
  yum -q list installed "$1" 2>/dev/null | grep -q "$1" && {
    log_ln 'Package '"'""$1""'"' already installed'
    return 1
  } || {
    yum -y install "$1" >> $LOG_FILE 2>&1 && {
      shout 'Installed package '"'""$1""'"
      return 0
    } || {
      shout 'Unable to install package '"'""$1""'"
      return 1
    }
  }
}

function install_group {
  yum -q group list installed "$1" 2>/dev/null | grep -q "$1" && {
    log_ln 'Group '"'""$1""'"' already installed'
    return 1
  } || {
    yum -y groups install "$1" >> $LOG_FILE 2>&1 && {
      shout 'Installed group '"'""$1""'"
      return 0
    } || {
      shout 'Unable to install group '"'""$1""'"
      return 1
    }
  }
}

function enable_service {
  local UNIT="$1"'.service'
  systemctl -q is-enabled "$UNIT" 2>/dev/null && {
    log_ln 'Service '"'""$UNIT""'"' already enabled'
    return 1
  } || {
    systemctl enable "$UNIT" >> $LOG_FILE 2>&1 && {
      shout 'Enabled service '"'""$UNIT""'"
      return 0
    } || {
      shout 'Unable to enable service '"'""$UNIT""'"
      return 1
    }
  }
}

function start_service {
  local UNIT="$1"'.service'
  systemctl -q is-active "$UNIT" 2>/dev/null && {
    log_ln 'Service '"'""$UNIT""'"' already started'
    return 1
  } || {
    systemctl start "$UNIT" >> $LOG_FILE 2>&1 && {
      shout 'Started service '"'""$UNIT""'"
      return 0
    } || {
      shout 'Unable to start service '"'""$UNIT""'"
      return 1
    }
  }
}

function start_provisioner {
  [ -f $LOG_FILE ] || touch $LOG_FILE
  echo 'Note: See verbose output using `tail -f '"${LOG_FILE##*/}"'`'
  log_ln 'Provisioner started...'
}

function end_provisioner {
  log_ln 'Provisioner ended.'
  [ $(file_lines $LOG_FILE) -le $LOG_MAX_LINES ] || {
    echo '-- TRUNCATED --' > $LOG_FILE.new \
      && tail -n $(( $LOG_MAX_LINES - 1 )) $LOG_FILE >> $LOG_FILE.new \
      && mv -f $LOG_FILE.new $LOG_FILE
  }
}

# ------------------------------------------------------------------------------
# PROVISIONER
# ------------------------------------------------------------------------------

start_provisioner

# GENERAL

shout 'General configuration...'

set_time_zone 'UTC'
update_system
install_package 'yum-utils'
install_package 'epel-release' && update_system
install_group 'Development Tools'
install_package 'ntp'
enable_service 'ntpd'
start_service 'ntpd'

# DOCKER

shout 'Docker configuration...'

add_repo 'https://download.docker.com/linux/centos/docker-ce.repo' && make_yum_cache
install_package 'docker-ce'
enable_service 'docker'
start_service 'docker'
add_user_to_group 'vagrant' 'docker'

# CLEANUP

clean_yum_cache

end_provisioner
