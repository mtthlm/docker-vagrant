# -*- mode: ruby -*-
# vi: set ft=ruby :
# ------------------------------------------------------------------------------
# CONFIGURATION
# ------------------------------------------------------------------------------

vm_cpus = 4
vm_memory = 4096
vm_aliases = []

# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------

def get_vbox_property(property, id)
  stdout = `VBoxManage guestproperty get #{id} '#{property}'`
  unless stdout == "No value set!"
    return stdout.split()[1]
  end
end

def vm_sudo(command, vm)
  stdout = ""
  vm.communicate.sudo(command) do |type, data|
    stdout << data if type == :stdout
  end
  return stdout.strip
end

def format_ip(string)
  return string[/(\d+\.\d+\.\d+\.\d+)/, 1]
end

def resolve_ip_vbox(vm, interface = "1")
  if vm.id
    property = get_vbox_property("/VirtualBox/GuestInfo/Net/#{interface}/V4/IP", vm.id)
    return format_ip(property)
  end
end

def resolve_ip_centos(vm, interface = "1")
  if vm.ssh_info && vm.ssh_info[:host]
    ip = vm_sudo("ip address show eth#{interface} 2>/dev/null | grep -A 2 'state UP' | tail -n 1 | awk '{print $2}' | cut -d '/' -f 1", vm)
    return format_ip(ip)
  end
end

# ------------------------------------------------------------------------------
# VAGRANT
# ------------------------------------------------------------------------------

Vagrant.configure("2") do |config|
  config.hostmanager.enabled = true

  config.vm.define "docker", primary: true do |docker|
    docker.vm.box = "centos/7"
    docker.vm.hostname = "docker"
    docker.vm.network "private_network", type: "dhcp"
    docker.vm.synced_folder "./", "/vagrant", type: "virtualbox"
    docker.vm.provider "virtualbox" do |virtualbox|
      virtualbox.name = "docker"
      virtualbox.cpus = vm_cpus
      virtualbox.memory = vm_memory
    end
    docker.vm.provision "shell", path: "provisioner.sh", run: "always"
    docker.hostmanager.aliases = vm_aliases
    docker.hostmanager.manage_host = true
    docker.hostmanager.manage_guest = false
    docker.hostmanager.ip_resolver = proc do |vm|
      resolve_ip_centos(vm) || resolve_ip_vbox(vm)
    end
  end
end
